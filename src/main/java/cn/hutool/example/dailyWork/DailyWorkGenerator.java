package cn.hutool.example.dailyWork;

import java.awt.Font;
import java.io.File;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.word.Word07Writer;

/**
 * 工作日报生成器<br>
 * 此生成器根据预定义好的内容，生成Word文件，做为邮件的附件。
 * 
 * @author Looly
 *
 */
public class DailyWorkGenerator {
	/** 标题字体 */
	private static final Font TITLE_FONT = new Font("黑体", Font.PLAIN, 22);
	/** 正文字体 */
	private static final Font MAIN_FONT = new Font("宋体", Font.PLAIN, 14);
	
	
	/**
	 * 生成日报
	 * 
	 * @return 日报word文件
	 */
	public static File generate() {
		// 1、准备文件
		File wordFile = FileUtil.file(StrUtil.format("每日工作汇报_{}.docx", DateUtil.today()));
		if(FileUtil.exist(wordFile)) {
			// 如果文件存在，删除之（可能上次发送遗留）
			wordFile.delete();
		}
		
		// 生成并写出word
		Word07Writer writer = new Word07Writer(wordFile);
		writer.addText(ParagraphAlignment.CENTER, TITLE_FONT, "工作日报");
		writer.addText(MAIN_FONT, "");
		writer.addText(MAIN_FONT, "尊敬的领导：");
		writer.addText(MAIN_FONT, "    今天我在Hutool群里摸鱼，什么工作也没做。");
		
		writer.close();
		return wordFile;
	}
}
